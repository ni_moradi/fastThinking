package com.nimamoradi.fasttinking.Data;

/**
 * Created by nima on 10/19/2016.
 */
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }
    private int ID;
    private String Table;


    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }

        return instance;
    }




    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read all pic data from the database.
     *
     * @return a List of pic data
     */
    public List<String> getPicData( byte[] image,int Id,String Table) {
        List<String> list = new ArrayList<>();
//"SELECT * FROM "+Table+" WHERE "+Table+".ID="+"1".trim()
        Cursor cursor = database.rawQuery("SELECT * FROM "+Table+" WHERE "+Table+".ID="+"1".trim(), null);
        cursor.moveToFirst();

            list.add(cursor.getString(0));
            list.add(cursor.getString(1));
            list.add(cursor.getString(2));
            list.add(cursor.getString(3));
        list.add(cursor.getString(4));

           image=cursor.getBlob(5);


        cursor.close();
        return list;
    }

}