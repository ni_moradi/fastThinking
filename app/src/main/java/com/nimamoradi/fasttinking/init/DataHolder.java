package com.example.mahdi.fastthinking.init;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import java.util.Random;

/**
 * Created by nima on 10/20/2016.
 */
public class DataHolder {

    Context getContext;
    DataHolder(Context getContext){this.getContext=getContext;}


    protected byte[] image;

    String answer;

    String wrong_answer;

    String wrong_answer2;

    String wrong_answer3;

    public String getAnswer() {
        return answer;
    }

    public String getWrongAnswer() {
        Random random = new Random();
        switch (random.nextInt(3)) {
            case 0:
                return wrong_answer;
            case 1:
                return wrong_answer2;
            case 2:
                return wrong_answer3;

        }
        return wrong_answer;
    }

    private Bitmap getImage() {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public ImageView getImageView() {
        ImageView imageView = new ImageView(getContext);
        imageView.setImageBitmap(getImage());
        return imageView;
    }




}
