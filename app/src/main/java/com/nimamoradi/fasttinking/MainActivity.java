package com.nimamoradi.fasttinking;

import android.app.VoiceInteractor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nimamoradi.fasttinking.Data.DatabaseAccess;
import com.nimamoradi.fasttinking.init.DataHolder;
import com.nimamoradi.fasttinking.init.DataInit;
import com.nimamoradi.fasttinking.init.DataRetrive;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
setContentView(R.layout.activity_main);
        DataInit dataInit=new DataInit(MainActivity.this);
        DataHolder i=dataInit.getData("english",1);
        TextView view= (TextView) findViewById(R.id.text);
view.setText(i.getAnswer());


        ImageView imageView= (ImageView) findViewById(R.id.image);
        ImageView tempimageView=i.getImageView();
        tempimageView.setId(R.id.image);


        // auto-match criteria to invite one random automatch opponent.
        // You can also specify more opponents (up to 3).
//
//        Bundle am = RoomConfig.createAutoMatchCriteria(1, 1, 0);
//
//        // build the room config:
//        RoomConfig.Builder roomConfigBuilder = makeBasicRoomConfigBuilder();
//        roomConfigBuilder.setAutoMatchCriteria(am);
//        RoomConfig roomConfig = roomConfigBuilder.build();
//
//        // create room:
//        Games.RealTimeMultiplayer.create(mGoogleApiClient, roomConfig);
//
//        // prevent screen from sleeping during handshake
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//
//        // go to game screen
        }

    }

